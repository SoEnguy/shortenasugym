var Encore = require('@symfony/webpack-encore');

var StyleLintPlugin = require('stylelint-webpack-plugin');

Encore
// directory where all compiled assets will be stored
    .setOutputPath('sources/public/assets')

    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/assets')

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // will output as web/build/global.css
    .addStyleEntry('global', './sources/assets/scss/global.scss')

    // allow post css loader (autoprefixer)
    .enablePostCssLoader()

    .addPlugin(new StyleLintPlugin())

    // allow sass/scss files to be processed
    .enableSassLoader()

    .enableSourceMaps(!Encore.isProduction())

    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning()
;

var config = Encore.getWebpackConfig();
config.watchOptions = {
    poll: true,
    ignored: /node_modules/
};

// export the final configuration
module.exports = config;
