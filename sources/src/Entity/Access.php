<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccessRepository")
 */
class Access
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $user_agent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShortUrl",inversedBy="access")
     */
    private $short_url;

    /**
     * Access constructor.
     */
    public function __construct($short_url)
    {
        $this->setShortUrl($short_url);
        $this->setDate(new \DateTime());
        $this->setUserAgent($_SERVER['HTTP_USER_AGENT']);

        if (!empty($_SERVER['HTTP_X_REAL_IP'])):
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) :
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) :
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else :
            $ip = $_SERVER['REMOTE_ADDR'];
        endif;

        $this->setIp($ip);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     * @return Access
     */
    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param null|string $ip
     * @return Access
     */
    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getUserAgent(): ?string
    {
        return $this->user_agent;
    }

    /**
     * @param null|string $user_agent
     * @return Access
     */
    public function setUserAgent(?string $user_agent): self
    {
        $this->user_agent = $user_agent;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getShortUrl(): ?int
    {
        return $this->short_url_id;
    }

    /**
     * @param int $short_url_id
     * @return Access
     */
    public function setShortUrl(ShortUrl $short_url): self
    {
        $this->short_url = $short_url;

        return $this;
    }
}
