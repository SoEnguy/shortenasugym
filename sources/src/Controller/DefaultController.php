<?php

namespace App\Controller;

use App\Entity\Access;
use App\Entity\ShortUrl;
use Doctrine\Bundle\DoctrineBundle\ManagerConfigurator;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * Shows up the index page (form + stats)
     */
    public function index(Request $request)
    {
        $url = new ShortUrl();
        $url->setHits(0);

        $form = $this->createFormBuilder($url)
            ->add('short_url', TextType::class)
            ->add('long_url', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create shortened URL'))
            ->getForm();

        $form->handleRequest($request);

        /** If it's a valid form submission, we register the URL, and redirect to another route */
        if ($form->isSubmitted() && $form->isValid()) {
            $url = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($url);
            $em->flush();

            return $this->redirectToRoute('created', ['short_url' => $url->getShortUrl()]);
        }

        $short_url_repo = $this->getDoctrine()->getRepository(ShortUrl::class);

        return $this->render(
            'home.html.twig',
            [
                'form' => $form->createView(),
                'urls' => $short_url_repo->findAll()
            ]
        );
    }

    /**
     * Handles every URL that is not index
     */
    public function redirectHandler(Request $request, $short_url)
    {
        $repo = $this->getDoctrine()->getRepository(ShortUrl::class);

        /**
         * @var ShortUrl|null $result
         */
        $result = $repo->findOneBy(
            ['short_url' => $short_url]
        );

        if ($result) {
            $em = $this->getDoctrine()->getManager();

            $result->incrementHits();
            $access = new Access($result);

            $em->persist($result);
            $em->persist($access);
            $em->flush();

            return $this->redirect($result->getLongUrl());
        }

        throw new NotFoundHttpException('There is no shortened URL with ' . $short_url);
    }

    public function created(Request $request)
    {
        $short_url = $request->get('short_url');

        /** Handles access to the "created" page without a parameter so that it's handled as a shortened URL */
        if (!$short_url) {
            return $this->redirectHandler($request, 'created');
        }

        return $this->render('created.html.twig', ['short_url' => $short_url]);
    }
}