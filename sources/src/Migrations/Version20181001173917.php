<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181001173917 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE access (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, short_url_id INTEGER DEFAULT NULL, date DATETIME NOT NULL, ip VARCHAR(255) DEFAULT NULL, user_agent VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_6692B54F1252BC8 ON access (short_url_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__short_url AS SELECT id, short_url, long_url, hits FROM short_url');
        $this->addSql('DROP TABLE short_url');
        $this->addSql('CREATE TABLE short_url (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, short_url VARCHAR(255) NOT NULL COLLATE BINARY, long_url VARCHAR(255) NOT NULL COLLATE BINARY, hits INTEGER DEFAULT NULL, created_at DATE NOT NULL)');
        $this->addSql('INSERT INTO short_url (id, short_url, long_url, hits) SELECT id, short_url, long_url, hits FROM __temp__short_url');
        $this->addSql('DROP TABLE __temp__short_url');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE access');
        $this->addSql('CREATE TEMPORARY TABLE __temp__short_url AS SELECT id, short_url, long_url, hits FROM short_url');
        $this->addSql('DROP TABLE short_url');
        $this->addSql('CREATE TABLE short_url (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, short_url VARCHAR(255) NOT NULL, long_url VARCHAR(255) NOT NULL, hits INTEGER DEFAULT 0)');
        $this->addSql('INSERT INTO short_url (id, short_url, long_url, hits) SELECT id, short_url, long_url, hits FROM __temp__short_url');
        $this->addSql('DROP TABLE __temp__short_url');
    }
}
