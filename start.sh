#!/bin/bash

containers=$(docker ps -q)

if [ -n "$containers" ]
then
        echo "Arrêt des containers"
	        docker stop $containers
		else
		        echo "Pas de containers à arrêter"
			fi

			echo "Lancement du compose en detached"
			docker-compose up -d
